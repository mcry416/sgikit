/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

// MARK: - LayoutMaker

public class LayoutMaker {
    
    public lazy var leading  = LayoutXAxisProperty(view: view, axisType: .leading)
    public lazy var trailing = LayoutXAxisProperty(view: view, axisType: .trailing)
    public lazy var left     = LayoutXAxisProperty(view: view, axisType: .left)
    public lazy var right    = LayoutXAxisProperty(view: view, axisType: .right)
    public lazy var centerX  = LayoutXAxisProperty(view: view, axisType: .centerX)
    public lazy var top      = LayoutYAxisProperty(view: view, axisType: .top)
    public lazy var bottom   = LayoutYAxisProperty(view: view, axisType: .bottom)
    public lazy var centerY  = LayoutYAxisProperty(view: view, axisType: .centerY)
    public lazy var width    = LayoutDimensionProperty(view: view, dimensionType: .width)
    public lazy var height   = LayoutDimensionProperty(view: view, dimensionType: .height)
    public lazy var size     = LayoutSizeProperty(view: view)
    public lazy var edges    = LayoutEdgesProperty(view: view)
    
    internal var activate: [NSLayoutConstraint] = [NSLayoutConstraint]()
    
    private let view: UIView

    fileprivate init(view: UIView) {
        self.view = view
        
        leading.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        trailing.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        left.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        right.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        centerX.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        top.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        bottom.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        centerY.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        width.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        height.didConstraintBlock = { [weak self] constraint in
            guard let `self` = self else { return }
            self.activate.append(constraint)
        }
        size.didConstraintBlock = { [weak self] constraints in
            guard let `self` = self else { return }
            constraints.forEach{ self.activate.append ($0) }
        }
        edges.didConstraintBlock = { [weak self] constraints in
            guard let `self` = self else { return }
            constraints.forEach{ self.activate.append( $0) }
        }
    }
    
}

fileprivate class GramKey {
    internal static var gram_activate_key: Int32 = 151988
}

extension UIView {
    
    internal var _gram_activate: [NSLayoutConstraint]? {
        set { objc_setAssociatedObject(self, &GramKey.gram_activate_key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
        get { objc_getAssociatedObject(self, &GramKey.gram_activate_key) as? [NSLayoutConstraint] }
    }
    
}

// MARK: - gram.layout

public protocol GramProtocol { }

public class GramNamespace<T>: GramProtocol {
    
    public typealias WrappedType = T
    
    public let base: T
    init(_ base: T) {
        self.base = base
    }
    
}

extension GramProtocol {
    
    public var gram: GramNamespace<Self> {
        get { GramNamespace(self) }
        set {}
    }
    
}

extension UIView: GramProtocol { }

extension GramNamespace where T: UIView {
    
    public func layout(_ closure: (_ make: LayoutMaker) -> Void) {
        assert(self.base.superview != nil, ASSERT_LAYOUT_SUPERVIEW_ERROR)
        self.base.translatesAutoresizingMaskIntoConstraints = false
        let layoutMaker = LayoutMaker(view: self.base)
        closure(layoutMaker)
        self.base._gram_activate = layoutMaker.activate
        NSLayoutConstraint.activate(layoutMaker.activate)
    }

    public func removeLayout() {
        guard let activate = self.base._gram_activate else { return }
        activate.forEach { $0.isActive = false }
        NSLayoutConstraint.deactivate(activate)
        self.base.removeConstraints(activate)
        if let superView = self.base.superview {
            for tempConstraint in superView.constraints {
                if tempConstraint.firstItem as? UIView == self.base {
                    superView.removeConstraint(tempConstraint)
                }
            }
        }
        self.base._gram_activate?.removeAll()
    }
    
    public func updateLayout(_ closure: (_ make: LayoutMaker) -> Void) {
        if var activate = self.base._gram_activate {
            let layoutMaker = LayoutMaker(view: self.base)
            closure(layoutMaker)
            for (oldIdx, currentSubConstraint) in activate.enumerated() {
                for newSubConstraint in layoutMaker.activate {
                    if newSubConstraint.firstAnchor == currentSubConstraint.firstAnchor {
                        activate[oldIdx].isActive = false
                        activate[oldIdx] = newSubConstraint
                        activate[oldIdx].isActive = true
                    }
                }
            }
        } else {
            layout(closure)
        }
    }
    
    public var leading: NSLayoutXAxisAnchor {
        base.leadingAnchor
    }
    
    public var trailing: NSLayoutXAxisAnchor {
        base.trailingAnchor
    }
    
    public var left: NSLayoutXAxisAnchor {
        base.leftAnchor
    }
    
    public var right: NSLayoutXAxisAnchor {
        base.rightAnchor
    }
    
    public var centerX: NSLayoutXAxisAnchor {
        base.centerXAnchor
    }
    
    public var top: NSLayoutYAxisAnchor {
        base.topAnchor
    }
    
    public var bottom: NSLayoutYAxisAnchor {
        base.bottomAnchor
    }
    
    public var centerY: NSLayoutYAxisAnchor {
        base.centerYAnchor
    }
    
    public var width: NSLayoutDimension {
        base.widthAnchor
    }
    
    public var height: NSLayoutDimension {
        base.heightAnchor
    }

}
