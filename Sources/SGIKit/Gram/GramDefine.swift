/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Foundation

#if os(iOS) || os(tvOS)
    import UIKit
#else
    import AppKit
#endif

#if os(iOS) || os(tvOS)
    public typealias ConstraintView = UIView
#else
    public typealias ConstraintView = NSView
#endif

internal let FATAL_LAYOUT_PARA_ERROR: String = "a fatal error occurred during the constraint period, and the reason for exception was that incorrect constraint object was passed in."

internal let ASSERT_LAYOUT_SUPERVIEW_ERROR: String = "before making constraints, the view must be added into a superview!"

public protocol GramConstraintItem { }

extension Int: GramConstraintItem { }

extension Double: GramConstraintItem { }

extension CGSize: GramConstraintItem { }

extension CGFloat: GramConstraintItem { }

extension ConstraintView: GramConstraintItem { }

extension NSLayoutDimension: GramConstraintItem { }

extension NSLayoutXAxisAnchor: GramConstraintItem { }

extension NSLayoutYAxisAnchor: GramConstraintItem { }


