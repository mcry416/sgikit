/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

public class LayoutDimensionProperty {
    
    enum DimensionType {
        case width
        case height
    }
    
    internal var didConstraintBlock: ((NSLayoutConstraint) -> Void)?
    private let view: UIView
    private let dimensionType: DimensionType
    
    init(view: UIView, dimensionType: DimensionType) {
        self.view = view
        self.dimensionType = dimensionType
    }
    
    public func equalTo(_ item: GramConstraintItem) {
        var constraint: NSLayoutConstraint?
        
        if let value = item as? Int {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(equalToConstant: CGFloat(value))
            case .height:
                constraint = view.heightAnchor.constraint(equalToConstant: CGFloat(value))
            }
        }
        
        if let value = item as? Double {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(equalToConstant: CGFloat(value))
            case .height:
                constraint = view.heightAnchor.constraint(equalToConstant: CGFloat(value))
            }
        }
        
        if let value = item as? CGFloat {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(equalToConstant: value)
            case .height:
                constraint = view.heightAnchor.constraint(equalToConstant: value)
            }
        }
        
        if let value = item as? NSLayoutDimension {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(equalTo: value)
            case .height:
                constraint = view.heightAnchor.constraint(equalTo: value)
            }
        }
        
        if let value = item as? ConstraintView {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(equalTo: value.widthAnchor)
            case .height:
                constraint = view.heightAnchor.constraint(equalTo: value.heightAnchor)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func equalToSuperView() {
        var constraint: NSLayoutConstraint?
        
        if let superView = view.superview {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(equalTo: superView.widthAnchor)
            case .height:
                constraint = view.heightAnchor.constraint(equalTo: superView.heightAnchor)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func greaterThanOrEqualTo(_ item: GramConstraintItem, offset: CGFloat = 0) {
        var constraint: NSLayoutConstraint?
        
        if let value = item as? Int {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(greaterThanOrEqualToConstant: CGFloat(value))
            case .height:
                constraint = view.heightAnchor.constraint(greaterThanOrEqualToConstant: CGFloat(value))
            }
        }
        
        if let value = item as? Double {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(equalToConstant: CGFloat(value))
            case .height:
                constraint = view.heightAnchor.constraint(equalToConstant: CGFloat(value))
            }
        }
        
        if let value = item as? CGFloat {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(greaterThanOrEqualToConstant: value)
            case .height:
                constraint = view.heightAnchor.constraint(greaterThanOrEqualToConstant: value)
            }
        }
        
        if let value = item as? NSLayoutDimension {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            case .height:
                constraint = view.heightAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func lessThanOrEqualTo(_ item: GramConstraintItem, offset: CGFloat = 0) {
        var constraint: NSLayoutConstraint?
        
        if let value = item as? Int {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(lessThanOrEqualToConstant: CGFloat(value))
            case .height:
                constraint = view.heightAnchor.constraint(lessThanOrEqualToConstant: CGFloat(value))
            }
        }
        
        if let value = item as? Double {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(equalToConstant: CGFloat(value))
            case .height:
                constraint = view.heightAnchor.constraint(equalToConstant: CGFloat(value))
            }
        }
        
        if let value = item as? CGFloat {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(lessThanOrEqualToConstant: value)
            case .height:
                constraint = view.heightAnchor.constraint(lessThanOrEqualToConstant: value)
            }
        }
        
        if let value = item as? NSLayoutDimension {
            switch dimensionType {
            case .width:
                constraint = view.widthAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            case .height:
                constraint = view.heightAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }

}
