/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

public class LayoutEdgesProperty {
    
    internal var didConstraintBlock: (([NSLayoutConstraint]) -> Void)?
    private let view: UIView
    
    init(view: UIView) {
        self.view = view
    }
    
    public func equalToSuperView(offset: CGFloat = 0) {
        if let superview = view.superview {
            equalTo(superview, offset: offset)
        }
    }
    
    public func equalTo(_ item: GramConstraintItem, offset: CGFloat = 0) {
        var constraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
        
        if let value = item as? ConstraintView {
            let leftConstraint = view.leftAnchor.constraint(equalTo: value.leftAnchor, constant: offset)
            let rightConstraint = view.rightAnchor.constraint(equalTo: value.rightAnchor, constant: -offset)
            let topConstraint = view.topAnchor.constraint(equalTo: value.topAnchor, constant: offset)
            let bottomConstraint = view.bottomAnchor.constraint(equalTo: value.bottomAnchor, constant: -offset)
            constraints.append(leftConstraint)
            constraints.append(rightConstraint)
            constraints.append(topConstraint)
            constraints.append(bottomConstraint)
        }
        
        didConstraintBlock?(constraints)
    }
    
}
