/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

public class LayoutYAxisProperty {
    
    enum YAxisType {
        case top
        case bottom
        case centerY
    }
    
    internal var didConstraintBlock: ((NSLayoutConstraint) -> Void)?
    private let view: UIView
    private let axisType: YAxisType
    
    init(view: UIView, axisType: YAxisType) {
        self.view = view
        self.axisType = axisType
    }
    
    public func equalTo(_ item: GramConstraintItem, offset: CGFloat = 0) {
        var constraint: NSLayoutConstraint?
        
        if let value = item as? NSLayoutYAxisAnchor {
            switch axisType {
            case .top:
                constraint = view.topAnchor.constraint(equalTo: value, constant: offset)
            case .bottom:
                constraint = view.bottomAnchor.constraint(equalTo: value, constant: offset)
            case .centerY:
                constraint = view.centerYAnchor.constraint(equalTo: value, constant: offset)
            }
        }
        
        if let value = item as? ConstraintView {
            switch axisType {
            case .top:
                constraint = view.topAnchor.constraint(equalTo: value.topAnchor, constant: offset)
            case .bottom:
                constraint = view.bottomAnchor.constraint(equalTo: value.bottomAnchor, constant: offset)
            case .centerY:
                constraint = view.centerYAnchor.constraint(equalTo: value.centerYAnchor, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func equalToSuperView(offset: CGFloat = 0) {
        var constraint: NSLayoutConstraint?
        
        if let superView = view.superview {
            switch axisType {
            case .top:
                constraint = view.topAnchor.constraint(equalTo: superView.topAnchor, constant: offset)
            case .bottom:
                constraint = view.bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: offset)
            case .centerY:
                constraint = view.centerYAnchor.constraint(equalTo: superView.centerYAnchor, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func greaterThanOrEqualTo(_ item: GramConstraintItem, offset: CGFloat = 0){
        var constraint: NSLayoutConstraint?
        
        if let value = item as? NSLayoutYAxisAnchor {
            switch axisType {
            case .top:
                constraint = view.topAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            case .bottom:
                constraint = view.bottomAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            case .centerY:
                constraint = view.centerYAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            }
        }
        
        if let value = item as? ConstraintView {
            switch axisType {
            case .top:
                constraint = view.topAnchor.constraint(greaterThanOrEqualTo: value.topAnchor, constant: offset)
            case .bottom:
                constraint = view.bottomAnchor.constraint(greaterThanOrEqualTo: value.bottomAnchor, constant: offset)
            case .centerY:
                constraint = view.centerYAnchor.constraint(greaterThanOrEqualTo: value.centerYAnchor, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func lessThanOrEqualTo(_ item: GramConstraintItem, offset: CGFloat = 0){
        var constraint: NSLayoutConstraint?
        
        if let value = item as? NSLayoutYAxisAnchor {
            switch axisType {
            case .top:
                constraint = view.topAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            case .bottom:
                constraint = view.bottomAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            case .centerY:
                constraint = view.centerYAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            }
        }
        
        if let value = item as? ConstraintView {
            switch axisType {
            case .top:
                constraint = view.topAnchor.constraint(lessThanOrEqualTo: value.topAnchor, constant: offset)
            case .bottom:
                constraint = view.bottomAnchor.constraint(lessThanOrEqualTo: value.bottomAnchor, constant: offset)
            case .centerY:
                constraint = view.centerYAnchor.constraint(lessThanOrEqualTo: value.centerYAnchor, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
}
