/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

public class LayoutSizeProperty {
    
    internal var didConstraintBlock: (([NSLayoutConstraint]) -> Void)?
    private let view: UIView
    
    init(view: UIView) {
        self.view = view
    }
    
    public func equalTo(_ item: GramConstraintItem) {
        var constraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
        
        if let value = item as? CGSize {
            let widthConstraint = view.widthAnchor.constraint(equalToConstant: value.width)
            let heightConstraint = view.heightAnchor.constraint(equalToConstant: value.height)
            constraints.append(widthConstraint)
            constraints.append(heightConstraint)
        }
        
        if let value = item as? ConstraintView {
            let widthConstraint = view.widthAnchor.constraint(equalTo: value.widthAnchor)
            let heightConstraint = view.heightAnchor.constraint(equalTo: value.heightAnchor)
            constraints.append(widthConstraint)
            constraints.append(heightConstraint)
        }
        
        didConstraintBlock?(constraints)
    }
    
    public func equalToSuperView() {
        var constraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
        
        if let superView = view.superview {
            let widthConstraint = view.widthAnchor.constraint(equalTo: superView.widthAnchor)
            let heightConstraint = view.heightAnchor.constraint(equalTo: superView.heightAnchor)
            constraints.append(widthConstraint)
            constraints.append(heightConstraint)
        }
        
        didConstraintBlock?(constraints)
    }
    
}
