/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

public class LayoutXAxisProperty {
    
    enum AxisType {
        case leading
        case trailing
        case left
        case right
        case centerX
    }
    
    internal var didConstraintBlock: ((NSLayoutConstraint) -> Void)?
    private let view: UIView
    private let axisType: AxisType
    
    init(view: UIView, axisType: AxisType) {
        self.view = view
        self.axisType = axisType
    }
    
    public func equalTo(_ item: GramConstraintItem, offset: CGFloat = 0) {
        var constraint: NSLayoutConstraint?
        
        if let value = item as? NSLayoutXAxisAnchor {
            switch axisType {
            case .leading:
                constraint = view.leadingAnchor.constraint(equalTo: value, constant: offset)
            case .trailing:
                constraint = view.trailingAnchor.constraint(equalTo: value, constant: offset)
            case .left:
                constraint = view.leftAnchor.constraint(equalTo: value, constant: offset)
            case .right:
                constraint = view.rightAnchor.constraint(equalTo: value, constant: offset)
            case .centerX:
                constraint = view.centerXAnchor.constraint(equalTo: value, constant: offset)
            }
        }
        
        if let value = item as? ConstraintView {
            switch axisType {
            case .leading:
                constraint = view.leadingAnchor.constraint(equalTo: value.leadingAnchor, constant: offset)
            case .trailing:
                constraint = view.trailingAnchor.constraint(equalTo: value.trailingAnchor, constant: offset)
            case .left:
                constraint = view.leftAnchor.constraint(equalTo: value.leftAnchor, constant: offset)
            case .right:
                constraint = view.rightAnchor.constraint(equalTo: value.rightAnchor, constant: offset)
            case .centerX:
                constraint = view.centerXAnchor.constraint(equalTo: value.centerXAnchor, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func greaterThanOrEqualTo(_ item: GramConstraintItem, offset: CGFloat = 0){
        var constraint: NSLayoutConstraint?
        
        if let value = item as? NSLayoutXAxisAnchor {
            switch axisType {
            case .leading:
                constraint = view.leadingAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            case .trailing:
                constraint = view.trailingAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            case .left:
                constraint = view.leftAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            case .right:
                constraint = view.rightAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            case .centerX:
                constraint = view.centerXAnchor.constraint(greaterThanOrEqualTo: value, constant: offset)
            }
        }
        
        if let value = item as? ConstraintView {
            switch axisType {
            case .leading:
                constraint = view.leadingAnchor.constraint(greaterThanOrEqualTo: value.leadingAnchor, constant: offset)
            case .trailing:
                constraint = view.trailingAnchor.constraint(greaterThanOrEqualTo: value.trailingAnchor, constant: offset)
            case .left:
                constraint = view.leftAnchor.constraint(greaterThanOrEqualTo: value.leftAnchor, constant: offset)
            case .right:
                constraint = view.rightAnchor.constraint(greaterThanOrEqualTo: value.rightAnchor, constant: offset)
            case .centerX:
                constraint = view.centerXAnchor.constraint(greaterThanOrEqualTo: value.centerXAnchor, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func equalToSuperView(offset: CGFloat = 0) {
        var constraint: NSLayoutConstraint?
        
        if let superView = view.superview {
            switch axisType {
            case .leading:
                constraint = view.leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: offset)
            case .trailing:
                constraint = view.trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: offset)
            case .left:
                constraint = view.leftAnchor.constraint(equalTo: superView.leftAnchor, constant: offset)
            case .right:
                constraint = view.rightAnchor.constraint(equalTo: superView.rightAnchor, constant: offset)
            case .centerX:
                constraint = view.centerXAnchor.constraint(equalTo: superView.centerXAnchor, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
    public func lessThanOrEqualTo(_ item: GramConstraintItem, offset: CGFloat = 0){
        var constraint: NSLayoutConstraint?
        
        if let value = item as? NSLayoutXAxisAnchor {
            switch axisType {
            case .leading:
                constraint = view.leadingAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            case .trailing:
                constraint = view.trailingAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            case .left:
                constraint = view.leftAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            case .right:
                constraint = view.rightAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            case .centerX:
                constraint = view.centerXAnchor.constraint(lessThanOrEqualTo: value, constant: offset)
            }
        }
        
        if let value = item as? ConstraintView {
            switch axisType {
            case .leading:
                constraint = view.leadingAnchor.constraint(lessThanOrEqualTo: value.leadingAnchor, constant: offset)
            case .trailing:
                constraint = view.trailingAnchor.constraint(lessThanOrEqualTo: value.trailingAnchor, constant: offset)
            case .left:
                constraint = view.leftAnchor.constraint(lessThanOrEqualTo: value.leftAnchor, constant: offset)
            case .right:
                constraint = view.rightAnchor.constraint(lessThanOrEqualTo: value.rightAnchor, constant: offset)
            case .centerX:
                constraint = view.centerXAnchor.constraint(lessThanOrEqualTo: value.centerXAnchor, constant: offset)
            }
        }
        
        guard let constraint = constraint else { fatalError(FATAL_LAYOUT_PARA_ERROR) }
        didConstraintBlock?(constraint)
    }
    
}
