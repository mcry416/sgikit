/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

public protocol SGPresentationProtocol {
    var controllerHeight: CGFloat { get }
}

/// a base class of vc to write bottom view
open class SGPresentationViewController: UIViewController, SGPresentationProtocol {

    open var controllerHeight: CGFloat {
        0
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(presentBottomShouldHide), name: NSNotification.Name(SG_PresentBottomHideKey), object: nil)
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(SG_PresentBottomHideKey), object: nil)
    }
    
    @objc open func presentBottomShouldHide() {
        self.dismiss(animated: true, completion: nil)
    }
    
}


internal let SG_PresentBottomHideKey: String = "SG_ShouldHidePresentBottom"
/// use an instance to show the transition
open class PresentBottom: UIPresentationController {
    
    /// black layer
    open lazy var blackView: UIView = {
        let view = UIView()
        if let frame = self.containerView?.bounds {
            view.frame = frame
        }
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(sendHideNotification))
        view.addGestureRecognizer(gesture)
        return view
    }()
    
    /// value to control height of bottom view
    open var controllerHeight: CGFloat
    
    public override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        //get height from an objec of PresentBottomVC class
        if case let vc as SGPresentationViewController = presentedViewController {
            controllerHeight = vc.controllerHeight
        } else {
            controllerHeight = UIScreen.main.bounds.width
        }
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }
    
    /// add blackView to the container and let alpha animate to 1 when show transition will begin
    open override func presentationTransitionWillBegin() {
        blackView.alpha = 0
        containerView?.addSubview(blackView)
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 1
        }
    }
    
    /// let blackView's alpha animate to 0 when hide transition will begin.
    open override func dismissalTransitionWillBegin() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
        }
    }
    
    /// remove the blackView when hide transition end
    ///
    /// - Parameter completed: completed or no
    open override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            blackView.removeFromSuperview()
        }
    }
    
    /// define the frame of bottom view
    open override var frameOfPresentedViewInContainerView: CGRect {
        CGRect(x: 0,
               y: UIScreen.main.bounds.height - controllerHeight,
           width: UIScreen.main.bounds.width,
          height: controllerHeight)
    }
    
    @objc open func sendHideNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: SG_PresentBottomHideKey), object: nil)
    }
    
}

extension UIViewController: UIViewControllerTransitioningDelegate {
    
    public func sg_presentViewController(_ vc: SGPresentationViewController) {
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    // function refers to UIViewControllerTransitioningDelegate
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let present = PresentBottom(presentedViewController: presented, presenting: presenting)
        return present
    }
    
}

