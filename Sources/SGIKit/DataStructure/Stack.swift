/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Foundation

public class Stack<T> {
    
    private var elements = [T]()
    public init() {}
     
    public func pop() -> T? {
        self.elements.popLast()
    }
     
    public func push(element: T){
        self.elements.append(element)
    }
     
    public func peek() -> T? {
        self.elements.last
    }
     
    public func isEmpty() -> Bool {
        self.elements.isEmpty
    }
 
    public var count: Int {
        self.elements.count
    }
     
}

 
