/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Foundation

public class Queue<T> {
    
    private var data = [T]()
     
    public init() {}
 
    public func dequeue() -> T? {
        data.removeFirst()
    }
 
    public func peek() -> T? {
        data.first
    }
 
    public func enqueue(element: T) {
        data.append(element)
    }
 
    public func clear() {
        data.removeAll()
    }
 
    public var count: Int {
        data.count
    }
 
    public var capacity: Int {
        get { data.capacity }
        set { data.reserveCapacity(newValue) }
    }
 
    public func isFull() -> Bool {
        count == data.capacity
    }

    public func isEmpty() -> Bool {
        data.isEmpty
    }
         
}

