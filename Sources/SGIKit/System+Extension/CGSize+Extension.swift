/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Foundation

extension CGSize {
    
    ///  Device view's size suitable type.
    public enum PlatformValueType: CGFloat {
        /// on iPad or iPhone, paramter number will be regular.
        case phone      = 1.0
        /// on iPad or iPhone, paramter number will be plus a scale of `1.3`. And this enum type is designed for standard application UI.
        case versionPad = 1.3
        /// on iPad or iPhone, paramter number will be plus a scale of `1.5`
        case halfPad    = 1.5
        /// on iPad or iPhone, paramter number will be plus a scale of `2.0`
        case fullPad    = 2.0
    }
    
    /**
     Give a concrete number, and select a suiiable fit type to fit various device view's size.
     */
    public func fit(_ type: PlatformValueType) -> CGSize {
        if kIS_PHONE {
            return self
        } else if kIS_PAD {
            return CGSize(width: self.width * type.rawValue, height: self.height * type.rawValue)
        } else {
            return self
        }
    }
    
}
