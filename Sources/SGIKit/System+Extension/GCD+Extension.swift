/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Foundation

// MARK: - Once.
extension DispatchQueue {
    
    private static var _onceTokenDictionary: [String: String] = { [: ] }()
    
    static public func once(token: String, _ block: (() -> Void)){
        defer { objc_sync_exit(self) }
        objc_sync_enter(self)
        
        if _onceTokenDictionary[token] != nil {
            return
        }

        _onceTokenDictionary[token] = token
        block()
    }
    
}
