//
//  GSSettingsView.swift
//  CaptureWebResource
//
//  Created by Eldest's MacBook on 2024/7/25.
//
/*
import UIKit

open class GSSettingsView: UIView {
    
    open lazy var list: Array<GSSettingsModel> = Array<GSSettingsModel>()
    
    open var inset: CGFloat = 16 {
        didSet {
            layoutIfNeeded()
        }
    }
    
    private var didSwitchBlock: ((Int, Bool) -> Void)?
    private var didClickSelfBlock: ((Int) -> Void)?
    
    private var headerTitle: String?
    private var footerTitle: String?

    open lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .plain)
        view.register(GSSettinngsCell.self, forCellReuseIdentifier: "GSSettinngsCell")
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.isScrollEnabled = false
        return view
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor),
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

// MARK: - Event
extension GSSettingsView {
    
    @discardableResult
    public func fill(style: GSSetttingsStyle, mainTitle: String!, subTitle: String? = nil, indicatorResource: String = "", rightLabelTitle: String? = nil, rightSwitchStatus: Bool? = nil, height: CGFloat = 56) -> Self{
        list.append(GSSettingsModel(style: style, mainTitle: mainTitle, subTitle: subTitle, indicatorResource: indicatorResource, rightLabelTitle: rightLabelTitle, rightSwitchStatus: rightSwitchStatus, height: height))
        return self
    }
    
    @discardableResult
    public func fill(headerTitle: String? = nil, footerTitle: String? = nil) -> Self{
        self.headerTitle = headerTitle
        self.footerTitle = footerTitle
        return self
    }
    
    @discardableResult
    public func builder() -> Self{
        tableView.reloadData()
        return self
    }
    
    public func reload() {
        tableView.reloadData()
    }
    
    public func setOnClickCellListener(handler: ((Int) -> Void)?) {
        didClickSelfBlock = { idx in
            handler?(idx)
        }
    }
    
    public func setOnChangeSwitchListener(handler: ((Int, Bool) -> Void)?) {
        didSwitchBlock = { idx, isOn in
            handler?(idx, isOn)
        }
    }
    
    public func updateCell(index: Int, mainTitle: String? = nil, subTitle: String? = nil, indicatorResource: String? = nil, rightLabelTitle: String? = nil, rightSwitchStatus: Bool? = nil, height: CGFloat = 56) {
        guard let previous = list[safe: index] else {
#if DEBUG
            fatalError("a fatal error was happed during executing the method of updateCell(), reason: beyond the index at: \(index), normal range: 0..\(list.count)")
#endif
            return
        }
        let model: GSSettingsModel = GSSettingsModel(style: previous.style, mainTitle: mainTitle ?? previous.mainTitle, subTitle: subTitle, indicatorResource: indicatorResource, rightLabelTitle: rightLabelTitle, rightSwitchStatus: rightSwitchStatus, height: height)
        list[index] = model
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
    
}

// MARK: - Delegate
extension GSSettingsView: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        list.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        list[indexPath.row].height
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") else { return nil }
        guard let headerTitle else { return nil }
        headerView.textLabel?.text = headerTitle
        headerView.textLabel?.numberOfLines = 0
        return headerView
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FooterView") else { return nil }
        guard let footerTitle else { return nil }
        headerView.textLabel?.text = footerTitle
        headerView.textLabel?.numberOfLines = 0
        return headerView
    }
     
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerTitle != nil ? UITableView.automaticDimension : 0
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return footerTitle != nil ? UITableView.automaticDimension : 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GSSettinngsCell", for: indexPath) as? GSSettinngsCell {
            
            let model: GSSettingsModel = list[indexPath.row]
            cell.fill(style: model.style, mainTitle: model.mainTitle, subTitle: model.subTitle, indicatorResource: model.indicatorResource, rightLabelTitle: model.rightLabelTitle, rightSwitchStatus: model.rightSwitchStatus, inset: inset)
            cell.didClickSelfBlock = { [weak self] in
                guard let `self` = self else { return }
                self.didClickSelfBlock?(indexPath.row)
            }
            cell.didSwitchBlock = { [weak self] isOn in
                guard let `self` = self else { return }
                self.didSwitchBlock?(indexPath.row, isOn)
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
*/
