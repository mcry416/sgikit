//
//  GSSettinngsCell.swift
//  CaptureWebResource
//
//  Created by Eldest's MacBook on 2024/7/25.
//
/*
import UIKit

public enum GSSetttingsStyle: Int {
    case noneStyle   = 0
    case indicator   = 1
    case switchStyle = 2
    case moreLabel   = 3
}

open class GSSettinngsCell: UITableViewCell {
    
    public var didSwitchBlock: ((Bool) -> Void)?
    public var didClickSelfBlock: (() -> Void)?
    
    open lazy var mainTitleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14)
        label.textAlignment = .left
        return label
    }()
    
    open lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 10, weight: .light)
        return label
    }()
    
    open lazy var rightSwitch: UISwitch = {
        let view = UISwitch()
        
        return view
    }()
    
    open lazy var rightImageView: UIImageView = {
        let view = UIImageView()
        
        return view
    }()
    
    open lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        return label
    }()
    
    open lazy var divider: UIView = {
        let view = UIView()
        
        return view
    }()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubviews()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func prepareForReuse() {
        super.prepareForReuse()
        
        mainTitleLabel.removeConstraints(mainTitleLabel.constraints)
        subTitleLabel.removeConstraints(subTitleLabel.constraints)
        rightLabel.removeConstraints(rightLabel.constraints)
        rightSwitch.removeConstraints(rightSwitch.constraints)
        rightImageView.removeConstraints(rightImageView.constraints)
        
        resetStatus()
    }
    
}

// MARK: - View
extension GSSettinngsCell {
    
    private func setupSubviews() {
        contentView.setOnClickListener { [weak self] in
            guard let `self` = self else { return }
            self.didClickSelfBlock?()
        }
        contentView.addSubview(mainTitleLabel)
        contentView.addSubview(subTitleLabel)
        contentView.addSubview(rightLabel)
        contentView.addSubview(rightSwitch)
        contentView.addSubview(rightImageView)
        
        mainTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        rightLabel.translatesAutoresizingMaskIntoConstraints = false
        rightSwitch.translatesAutoresizingMaskIntoConstraints = false
        rightImageView.translatesAutoresizingMaskIntoConstraints = false
        
        resetStatus()
    }
    
    private func resetStatus() {
        mainTitleLabel.text = nil
        subTitleLabel.text = nil
        rightLabel.text = nil
        rightSwitch.isOn = false
        rightImageView.image = nil
        
        mainTitleLabel.isHidden = true
        subTitleLabel.isHidden = true
        rightLabel.isHidden = true
        rightSwitch.isHidden = true
        rightImageView.isHidden = true
    }
    
}

// MARK: - Fill
extension GSSettinngsCell {
    
    public func fill(style: GSSetttingsStyle = .noneStyle,
                     mainTitle: String,
                     subTitle: String? = nil,
                     indicatorResource: String? = nil,
                     rightLabelTitle: String? = nil,
                     rightSwitchStatus: Bool? = nil,
                     inset: CGFloat
    ) {
        if let subTitle {
            mainTitleLabel.text = mainTitle
            subTitleLabel.text = subTitle
            mainTitleLabel.isHidden = false
            subTitleLabel.isHidden = false
            
            NSLayoutConstraint.activate([
                mainTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: inset),
                mainTitleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
                mainTitleLabel.heightAnchor.constraint(equalToConstant: 20),
                
                subTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: inset),
                subTitleLabel.topAnchor.constraint(equalTo: mainTitleLabel.bottomAnchor, constant: 6)
            ])
            
        } else {
            mainTitleLabel.text = mainTitle
            mainTitleLabel.isHidden = false
            
            NSLayoutConstraint.activate([
                mainTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: inset),
                mainTitleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])
        }
        
        if let indicatorResource, !indicatorResource.isEmpty {
            rightImageView.image = UIImage(named: indicatorResource)
            rightImageView.isHidden = false
            
            NSLayoutConstraint.activate([
                rightImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -inset),
                rightImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])
        }
        
        if let rightLabelTitle {
            rightLabel.text = rightLabelTitle
            rightLabel.isHidden = false
            
            NSLayoutConstraint.activate([
                rightLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -inset),
                rightLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])
        }
        
        if let rightSwitchStatus {
            rightSwitch.isOn = rightSwitchStatus
            rightSwitch.isHidden = false
            
            NSLayoutConstraint.activate([
                rightSwitch.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -inset),
                rightSwitch.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])
        }
    }
    
}
*/
