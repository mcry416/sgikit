/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Foundation

open class SGMenu1Model: NSObject{
    
    public var title: String = ""
    
    public var imageName: String = ""
    
    public init(_ title: String, _ imageName: String) {
        self.title = title
        self.imageName = imageName
    }
}
