/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

open class SGMenuView: UIView {
    
    private let MENU_CELL_ID: String = "MENU_CELL_ID"
    /// SGMenuView's right and left padding for screen.
    private let padding: CGFloat = 12
    ///
    let bottomPadding: CGFloat = 40
    
    public var titleColor: UIColor?
    public lazy var contentView: UIView = self.createContentView()
    public lazy var collectionView: UICollectionView = self.createCollectionView()
    
    private var menuClickAction:      ((_ index: Int) -> Void)?
    private var menuLongClickAction:  ((_ index: Int) -> Void)?
    private var menuCloseAction:      (() -> Void)?
    
    public var datas: Array<SGMenu1Model> = Array<SGMenu1Model>()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = UIScreen.main.bounds
        self.backgroundColor = .black.withAlphaComponent(0.25)
        self.alpha = 0
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public convenience init(datas: Array<SGMenu1Model>) {
        self.init(frame: .zero)
        
        _initView(datas: datas)
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        guard let touch: UITouch = event?.touches(for: self)?.first else { return }
        let touchPoint: CGPoint = touch.location(in: contentView)
        
        let _: Bool = touchPoint.x >= contentView.frame.minX && touchPoint.x <= contentView.frame.maxX
        let _: Bool = touchPoint.y >= contentView.frame.minY && touchPoint.y <= contentView.frame.maxY
        
        self.hideSGMenuView()
    }
    
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let touch: UITouch = event?.touches(for: self)?.first else {
            return super.hitTest(point, with: event)
        }
        let touchPoint: CGPoint = touch.location(in: contentView)
        
        let isInX: Bool = touchPoint.x >= contentView.frame.minX && touchPoint.x <= contentView.frame.maxX
        let isInY: Bool = touchPoint.y >= contentView.frame.minY && touchPoint.y <= contentView.frame.maxY
        
        if isInX && isInY {
            return contentView
        }
        
        return super.hitTest(point, with: event)
    }

}

// MARK: - Listener.
extension SGMenuView{
    
    /**
     Listener the action when use click the menu item.
     - Parameter handler: Provide a index for user to operation.
     */
    public final func setOnMenuClickListener(handler: ((_ index: Int) -> Void)?){
        menuClickAction = { (index) in
            handler?(index)
        }
    }
    
    /**
     Listener the action when use close the menu view.
     */
    public final func setOnMenuCloseListener(handler: (() -> Void)?){
        menuCloseAction = {
            handler?()
        }
    }
    
    /**
     Close the menu view.
     */
    public final func close(){
        hideSGMenuView()
    }
    
}

// MARK: - UI.
extension SGMenuView{
    
    fileprivate func _initView(datas: Array<SGMenu1Model>){
        self.contentView.layer.cornerRadius = 10

        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(self)
        window.addSubview(contentView)
        contentView.addSubview(collectionView)
        
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
        
        self.datas = datas
        
        // Provide an original frame for contentView to show in the screen when it was showed.
        // It should be under the screen bottom to use the animation.
        let originalShowStageFrame: CGRect = CGRect(x: padding,
                                                    y: kScreenHeight(),
                                                    width: kScreenWidth() - (padding * 2),
                                                    height: 100)
        contentView.frame = originalShowStageFrame
        collectionView.frame = CGRect(origin: .zero, size: contentView.frame.size)
        
        
        // If data's count was less than 5, then use the min frame, otherwise use the max frame.
        var conditionFrame: CGRect = .zero
        if self.datas.count > 5 {
            conditionFrame = CGRect(x: padding,
                                y: kScreenHeight() - kBottomPadding() - 210,
                                width: kScreenWidth() - (padding * 2),
                                height: 210)
        } else {
            conditionFrame = CGRect(x: padding,
                                y: kScreenHeight() - kBottomPadding() - 110,
                                width: kScreenWidth() - (padding * 2),
                                height: 110)
        }
        
        // Data has been filled, set delegate.
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        // SGMenuView has been showed in the screen bottom, and now let it move to the position of `conditionFrame`.
        // Notice the collectionView also ought to decrease `yPadding` to let it to be beautiful.
        UIView.animate(withDuration: 0.618, delay: 0, usingSpringWithDamping: 0.618, initialSpringVelocity: 0.382, options: .curveEaseInOut) {
            let yPadding: CGFloat = 5
            self.contentView.frame = conditionFrame
            self.collectionView.frame = CGRect(x: 0,
                                               y: yPadding,
                                               width: self.contentView.frame.width,
                                               height: self.contentView.frame.height - yPadding * 2)
        } completion: { (true) in
            
        }

    }
    
    fileprivate func createCollectionView() -> UICollectionView{
        let flowLayout = UICollectionViewFlowLayout()
        // Make sure the cell in the screen has 5 only rathan using concretely frame.
        flowLayout.itemSize = CGSize(width: vWidth() / 6, height: (vWidth() / 6) + 27)
        
        let collectionView = UICollectionView(frame: CGRect(origin: .zero,
                                                            size: contentView.frame.size),
                                              collectionViewLayout: flowLayout)
        collectionView.register(SGMenu1Cell.self, forCellWithReuseIdentifier: MENU_CELL_ID)
        collectionView.backgroundColor = .white
        collectionView.isScrollEnabled = false
        
        return collectionView
    }
    
    fileprivate func createContentView() -> UIView{
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = true
        return view
    }
    
    @objc fileprivate func removeSGMenuView(){
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0
        } completion: { (true) in
            self.contentView.removeFromSuperview()
            self.removeFromSuperview()
        }
    }
    
    @objc public func hideSGMenuView(){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) { [self] in
            let hideStageFrame: CGRect = CGRect(x: self.padding,
                                                y: self.kScreenHeight(),
                                                width: self.kScreenWidth() - (self.padding * 2),
                                                height: 100)
            self.contentView.frame = hideStageFrame
            self.contentView.alpha = 0
        } completion: { (true) in
            
        }

        menuCloseAction?()
        self.perform(#selector(self.removeSGMenuView), with: nil, afterDelay: 0.3)
    }
    
    /// SGMenuView width.
    fileprivate func vWidth() -> CGFloat{
        return kScreenWidth() - (padding * 2)
    }
    /// SGMenuView height, gold rate.
    fileprivate func vHeight() -> CGFloat{
        return (vWidth() * 0.618)
    }
    /// Screen width.
    fileprivate func kScreenWidth() -> CGFloat{
        return UIScreen.main.bounds.width
    }
    /// Screen height.
    fileprivate func kScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.height
    }
    /// Device bottom padding for screen.
    fileprivate func kBottomPadding() -> CGFloat{
        return (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0) + 49
    }
    
}

// MARK: - UICollectionViewDataSource & Delegate.
extension SGMenuView: UICollectionViewDataSource, UICollectionViewDelegate{
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datas.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MENU_CELL_ID, for: indexPath) as! SGMenu1Cell
        
        cell.image.image = UIImage(named: datas[indexPath.row].imageName)
        cell.label.text = datas[indexPath.row].title
        cell.label.adjustsFontSizeToFitWidth = true
        if let color = titleColor {
            cell.label.textColor = color
        }
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        menuClickAction?(indexPath.row)
    }
    
}
