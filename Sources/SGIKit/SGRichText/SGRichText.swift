/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

open class SGRichText: NSObject {
    
    private var describeModel: [NSAttributedString.Key: Any] = { [NSAttributedString.Key: Any]() }()
    
    public var string: String!
    
    public init(_ string: String) {
        self.string = string
    }
    
    public func setBackgroundColor(_ color: UIColor) -> Self{
        self.describeModel[NSAttributedString.Key.backgroundColor] = color
        return self
    }
    
    public func setForegroundColorColor(_ color: UIColor) -> Self{
        self.describeModel[NSAttributedString.Key.foregroundColor] = color
        return self
    }
    
    public func setFont(_ font: UIFont) -> Self{
        self.describeModel[NSAttributedString.Key.font] = font
        return self
    }
    
    public func setShadow(shadowColor: UIColor, shadowRadius: CGFloat) -> Self{
        let shadow: NSShadow = NSShadow()
        shadow.shadowColor = shadowColor
        shadow.shadowBlurRadius = shadowRadius
        self.describeModel[NSAttributedString.Key.shadow] = shadow
        return self
    }
    
    public func builder() -> NSAttributedString{
        NSAttributedString(string: self.string, attributes: self.describeModel)
    }
    
}
