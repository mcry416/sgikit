/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
/*
import UIKit

public class SaltonseaBoot: NSObject, URLSessionDownloadDelegate {

    private lazy var config: URLSessionConfiguration = { URLSessionConfiguration.default }()
    
    private lazy var session: URLSession = { URLSession(configuration: config,
                                                        delegate: self,
                                                        delegateQueue: nil) }()
    
    private var setOnProgressBlock: ((_ rate: CGFloat) -> Void)?
    
    public static var shared: SaltonseaBoot = { SaltonseaBoot() }()
    
    open var interceptor: SaltonseaInterceptor?
    
    open var saltonseaConfiguration: SaltonseaRequestConfiguration?

    private override init() {
        super.init()
        
        initConfiguration()
    }
    
    deinit {
        
    }
    
    public func reuqestCenter(basicRequest: SaltonseaBasicRequest,
                              onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                              onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDataTask {
        var task: URLSessionDataTask = URLSessionDataTask()
        
        switch basicRequest.requestType {
            
        case .post:
            task = postRequest(urlString: saltonseaRequest.requestURLString,
                        parameters: saltonseaRequest.requestParameter,
                        retry: saltonseaRequest.retry) { data, response in
                onSuccess(data, response)
            } onFailure: { error in
                onFailure(error)
            }
        case .get:
            task = getRequest(urlString: saltonseaRequest.requestURLString,
                       parameters: saltonseaRequest.requestParameter,
                       timeoutInterval: saltonseaRequest.timeoutInterval,
                       retry: saltonseaRequest.retry) { data, response in
                onSuccess(data, response)
            } onFailure: { error in
                onFailure(error)
            }
        case .put:
            <#code#>
        case .delete:
            <#code#>
        case .head:
            <#code#>
        case .options:
            <#code#>
        case .patch:
            <#code#>
        }
        
        return task
    }
    
    @discardableResult
    public func sessionTaskCenter(saltonseaRequest: SaltonseaBasicRequest,
                                  onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                                  onFailure: @escaping ((_ error: Any?) -> Void?)) -> Self {
        switch saltonseaRequest.taskType {
        case .download:
            sessionTaskWithDownload(urlString: saltonseaRequest.requestURLString, parameters: saltonseaRequest.requestParameter, timeoutInterval: saltonseaRequest.timeoutInterval) { data, response in
                onSuccess(data, response)
            } onFailure: { error in
                 onFailure(error)
            }

        case .upload:
            guard saltonseaRequest.requestData != nil else { return self}
            sessionTaskWithUpload(urlString: saltonseaRequest.requestURLString, parameters: saltonseaRequest.requestParameter, timeoutInterval: saltonseaRequest.timeoutInterval, data: saltonseaRequest.requestData!) { data, response in
                onSuccess(data, response)
            } onFailure: { error in
                onFailure(error)
            }

        case .none:
            break
        }
        return self
    }
    
    public func setOnProgressListener(_ listener: ((_ rate: CGFloat) -> Void)?){
        setOnProgressBlock = { rate in
            listener?(rate)
        }
    }
    
}

extension SaltonseaBoot {
    
    public func postRequest(urlString: String,
                            parameters: [String : Any]?,
                            retry: Int,
                            onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                            onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDataTask {
        var task: URLSessionDataTask = URLSessionDataTask()
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
            request.httpMethod = "POST"
            if let interceptor = interceptor {
                request = interceptor.onRequest(urlRequest: request)
            }
            
            task = session.dataTask(with: request) { (tData, tResponse, tError) in
                guard tError != nil else {
                    onFailure(tError)
                    return
                }
                guard tData != nil, tResponse != nil else { return }
                onSuccess(tData, tResponse)
            }
            task.resume()
            return task
        } else {
            onFailure(SaltonseaError.urlError01)
        }
        return task
    }
    
    public func getRequest(urlString: String,
                           parameters: [String : Any]?,
                           timeoutInterval: TimeInterval,
                           retry: Int,
                           onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                           onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDataTask {
        var task: URLSessionDataTask = URLSessionDataTask()
        var tempURLString = urlString
        tempURLString = tempURLString + "?"
        parameters?.forEach({ (key: String, value: Any) in
            tempURLString = tempURLString + "\(key)=\(value)&"
        })
        (parameters?.count ?? 0) > 2 ? tempURLString.removeLast() : nil
        
        if let url = URL(string: tempURLString) {
            var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
            request.httpMethod = "GET"
            
            var tempRetry: Int = retry
            task = session.dataTask(with: request) { (data, response, error) in
                if error != nil && tempRetry == 0{
                    onFailure(error)
                    return
                } else {
                    self.getRequest(urlString: urlString, parameters: parameters, timeoutInterval: timeoutInterval, retry: tempRetry) { data, response in
                        
                    } onFailure: { error in
                        
                    }
                    tempRetry = tempRetry - 1
                    
                }

                guard data != nil, response != nil else { return }
                onSuccess(data, response)
            }
            task.resume()
            return task
        } else {
            onFailure(SaltonseaError.urlError01)
        }
        return task
    }
    
    public func sessionTaskWithUpload(basicRequest: SaltonseaBasicRequest,
                                      data: Data,
                                      onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                                      onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDataTask {
        var task: URLSessionDataTask = URLSessionDataTask()
        session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
            request.httpMethod = "POST"
            task = session.uploadTask(with: request, from: data) { (data, response, error) in
                if error != nil {
                    onFailure(error)
                    return
                }
                guard data != nil, response != nil else { return }
                onSuccess(data, response)
            }
            task.resume()
            return task
        } else {
            onFailure(SaltonseaError.urlError01)
        }
        return task
    }
    
    public func sessionTaskWithDownload(basicRequest: SaltonseaBasicRequest,
                                        onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                                        onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDownloadTask {
        var task: URLSessionDownloadTask = URLSessionDownloadTask()
        session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
//            request.httpMethod = "POST"
            task = session.downloadTask(with: request) { (data, response, error) in
                if error != nil {
                    onFailure(error)
                    return
                }
                guard data != nil, response != nil else { return }
                onSuccess(data, response)
            }
            task.resume()
            return task
        } else {
            onFailure(SaltonseaError.urlError01)
        }
        return task
    }
    
    func setOnStatusListener(urlCompatiableType: URLCompatibleType, listener: ((_ code: Int) -> Void)?) -> URLSessionDataTask {
        var task: URLSessionDataTask = URLSessionDataTask()
        session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        
        if let url = urlCompatiableType as? URL {
            
            task = session.dataTask(with: URLRequest(url: url)) { (data, response, error) in
                if let response = response as? HTTPURLResponse {
                    listener?(response.statusCode)
                } else {
                    listener?(999)
                }
            }
            task.resume()
            return task
        } else if let url = urlCompatiableType as? String {
            
            listener?(404)
        }
        return task
    }
    
}

extension SaltonseaBoot {
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
    }
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        
    }
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let written:CGFloat = (CGFloat)(totalBytesWritten)
        let total:CGFloat = (CGFloat)(totalBytesExpectedToWrite)
        let progress:CGFloat = written/total
        Log.warning(progress)
        
        setOnProgressBlock?(progress)
    }
    
}

extension SaltonseaBoot {
    
    private func initConfiguration() {
        guard let saltonseaConfiguration = saltonseaConfiguration else { return }
        if let timeoutIntervalForRequest = saltonseaConfiguration.timeoutIntervalForRequest {
            config.timeoutIntervalForRequest = timeoutIntervalForRequest
        }
        if let timeoutIntervalForResource = saltonseaConfiguration.timeoutIntervalForResource {
            config.timeoutIntervalForResource = timeoutIntervalForResource
        }
        if let httpAdditionalHeaders = saltonseaConfiguration.httpAdditionalHeaders {
            config.httpAdditionalHeaders = httpAdditionalHeaders
        }
        if let httpMaximumConnectionsPerHost = saltonseaConfiguration.httpMaximumConnectionsPerHost {
            config.httpMaximumConnectionsPerHost = httpMaximumConnectionsPerHost
        }
        if let httpCookieStorage = saltonseaConfiguration.httpCookieStorage {
            config.httpCookieStorage = httpCookieStorage
        }
        if let requestCachePolicy = saltonseaConfiguration.requestCachePolicy {
            config.requestCachePolicy = requestCachePolicy
        }
        
    }
    
}
*/
