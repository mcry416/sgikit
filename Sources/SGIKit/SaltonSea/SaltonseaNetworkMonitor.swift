/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Foundation
import Network

/**
 Saltonsea network monitor for network status.
 - Example usage:
 ```
 SaltonseaNetworkMonitor.shared.startMonitoring()
 
 SaltonseaNetworkMonitor.shared.setOnNetStatusChangeListener {
    // print interface type.
    print(SaltonseaNetworkMonitor.shared.interfaceType)
 }
 ```
 */
@available(macOS 10.14, iOS 12.0, watchOS 5.0, tvOS 12.0, *)
open class SaltonseaNetworkMonitor: NSObject {

    public static let shared: SaltonseaNetworkMonitor = SaltonseaNetworkMonitor()
    
    open var monitor: NWPathMonitor?
    
    open var isMonitoring: Bool = false
    
    private var didStartMonitoringBlock: (() -> Void)?
    
    private var didStopMonitoringBlock:  (() -> Void)?
    
    private var netStatusChangeBlock:    (() -> Void)?
    
    open var isConnected: Bool {
        guard let monitor = monitor else { return false }
        return monitor.currentPath.status == .satisfied
    }
    
    open var interfaceType: NWInterface.InterfaceType? {
        guard let monitor = monitor else { return nil }
        
        return monitor.currentPath.availableInterfaces.filter {
            monitor.currentPath.usesInterfaceType($0.type) }.first?.type
    }
    
    open var availableInterfacesTypes: [NWInterface.InterfaceType]? {
        guard let monitor = monitor else { return nil }
        return monitor.currentPath.availableInterfaces.map { $0.type }
    }
    
    open var isExpensive: Bool {
        return monitor?.currentPath.isExpensive ?? false
    }
    
    // MARK: - Init & Deinit
    
    private override init() {
        super.init()
    }
    
    deinit {
        stopMonitoring()
    }
    
    open func setOnStartMonitoringListener(_ block: (() -> Void)?) {
        didStartMonitoringBlock = {
            block?()
        }
    }
    
    open func setOnStopMonitoringListener(_ block: (() -> Void)?) {
        didStopMonitoringBlock = {
            block?()
        }
    }
    
    open func setOnNetStatusChangeListener(_ block: (() -> Void)?) {
        netStatusChangeBlock = {
            block?()
        }
    }
    
    open func startMonitoring() {
        guard !isMonitoring else { return }
        
        monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "sg_saltonsea_ntmt")
        monitor?.start(queue: queue)
        
        monitor?.pathUpdateHandler = { _ in
            self.netStatusChangeBlock?()
        }
        
        isMonitoring = true
        didStartMonitoringBlock?()
    }
    
    open func stopMonitoring() {
        guard isMonitoring, let monitor = monitor else { return }
        monitor.cancel()
        self.monitor = nil
        isMonitoring = false
        didStopMonitoringBlock?()
    }
    
}

