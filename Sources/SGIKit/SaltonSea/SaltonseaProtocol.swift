/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import Foundation

/**
 Saltonsea request protocol, using method of `request` to create a request model, and select a suitable way to build a task.
 - Features1: Provide listen method.
 */
protocol SaltonseaProtocol {
    
    /**
     Basic HTTP request method, which could using `post`, `get`, `put` method.
     - Parameter onSuccess: When data has been received data and executed this closure.
     - Parameter onFailure: When data parsed failed and executed this closure.
     */
    @discardableResult
    func dataTask(onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                  onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDataTask
    
    /**
     Sustainablity HTTP request until the task was ended.
     - Parameter onSuccess: When data has been received data and executed this closure.
     - Parameter onFailure: When data parsed failed and executed this closure.
     */
    @discardableResult
    func sessionTask(onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                     onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDownloadTask
    /**
     Listen the request status code.
     */
    @discardableResult
    func setOnStatusListener(listener: ((_ code: Int) -> Void)?) -> URLSessionDataTask
    
    @discardableResult
    func setOnProgressListener(_ listener: ((_ rate: CGFloat) -> Void)?) -> Self

}
