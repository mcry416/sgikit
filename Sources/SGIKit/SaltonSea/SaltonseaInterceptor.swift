/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

/**
 To unify the interception of request and response operations, inherit this class and override the methods of the parent class (**do not call the super method**) to implement your own business operations.
 - Example Usage :
 ```
class YourSaltonseaInterceptor: SaltonseaInterceptor {
    // ...
}
Saltonsea.shared.interceptor = YourSaltonseaInterceptor()
 ```
 
 */
open class SaltonseaInterceptor: NSObject {

    /**
     Override this method directly, do not call **`super.onRequest()`**.
     - Parameter urlRequest: a request class, to use the method of `addValue(, forHTTPHeaderField:)` to add a http head field. eg. add token for user authentication, and return this object at last.
     */
    open func onRequest(urlRequest: URLRequest) -> URLRequest {
        assert(false, "SaltonseaInterceptor sub-class must be override this method of `onRequest()`!")
        return urlRequest
    }
    
    /**
     Override this method directly, do not call **`super.onResponse()`**.
     */
    open func onResponse(response: Any) -> Any {
        assert(false, "SaltonseaInterceptor sub-class must be override this method of `onResponse()`!")
        return response
    }
    
    /**
     Override this method directly, do not call **`super.onError()`**.
     */
    open func onError(error: SaltonseaError) {
        assert(false, "SaltonseaInterceptor sub-class must be override this method of `onError()`!")
    }
    
}
