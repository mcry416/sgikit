/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

open class SaltonseaRequestConfiguration: NSObject {
    
    open var host: String?
    
    open var allowsCellularAccess: Bool?
    
    open var httpAdditionalHeaders: [AnyHashable : Any]?
    
    open var httpMaximumConnectionsPerHost: Int?

    open var httpCookieStorage: HTTPCookieStorage?
    
    open var requestCachePolicy: NSURLRequest.CachePolicy?

    open var timeoutIntervalForRequest: TimeInterval?

    open var timeoutIntervalForResource: TimeInterval?
    
    public init(host: String? = nil, allowsCellularAccess: Bool? = nil, httpAdditionalHeaders: [AnyHashable : Any]? = nil, httpMaximumConnectionsPerHost: Int? = nil, httpCookieStorage: HTTPCookieStorage? = nil, requestCachePolicy: NSURLRequest.CachePolicy? = nil, timeoutIntervalForRequest: TimeInterval? = nil, timeoutIntervalForResource: TimeInterval? = nil) {
        self.host = host
        self.allowsCellularAccess = allowsCellularAccess
        self.httpAdditionalHeaders = httpAdditionalHeaders
        self.httpMaximumConnectionsPerHost = httpMaximumConnectionsPerHost
        self.httpCookieStorage = httpCookieStorage
        self.requestCachePolicy = requestCachePolicy
        self.timeoutIntervalForRequest = timeoutIntervalForRequest
        self.timeoutIntervalForResource = timeoutIntervalForResource
     }
    
    public override init() {
        super.init()
        
    }
    
    open func addCookies(_ URL: Foundation.URL, newCookies: [String: String]) {
        for (k, v) in newCookies {
            if let cookie = HTTPCookie(properties: [
                HTTPCookiePropertyKey.name: k,
                HTTPCookiePropertyKey.value: v,
                HTTPCookiePropertyKey.originURL: URL,
                HTTPCookiePropertyKey.path: "/"
            ])
            {
                self.httpCookieStorage?.setCookie(cookie)
            }
        }
    }

}
