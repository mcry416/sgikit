/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
/*
import UIKit

/**
 A network componet that based on URLSessionTask. This is a user-layer which based on `SaltonseaProtocol` layer and user could replace the boot implments.
 */
open class Saltonsea: NSObject, SaltonseaProtocol {    
    
    /** A sington instance for itself */
    public static var shared: Saltonsea = { Saltonsea() }()
    
    /** A concrete boot network implements class to use, which could be replaced with other util to use, and it also formed with sington. */
    private lazy var boot: SaltonseaBoot = { SaltonseaBoot.shared }()
    
    public var interceptor: SaltonseaInterceptor? {
        didSet {
            boot.interceptor = interceptor
        }
    }
    
    public var saltonseaConfiguration: SaltonseaRequestConfiguration? {
        didSet {
            boot.saltonseaConfiguration = saltonseaConfiguration
        }
    }
    
    @syncVariable
    private var saltonseaBasicRequets: Stack<SaltonseaBasicRequest> = Stack<SaltonseaBasicRequest>()
    
    @syncVariable
    private var urlCompatiables: Stack<URLCompatibleType> = Stack<URLCompatibleType>()
    
    private override init() {
        super.init()
        
        _ = boot
    }
    
    open func post(_ urlCompatiableType: URLCompatibleType,
                   paras: [String: Any]? = nil,
                   retry: Int? = nil) -> Self {
        let request: SaltonseaBasicRequest = SaltonseaBasicRequest(requestType: .post, urlCompatiableType: urlCompatiableType, paras: paras, retry: retry)
        saltonseaBasicRequets.push(element: request)
        
        return self
    }
    
    open func get(_ urlCompatiableType: URLCompatibleType,
                  paras: [String: Any]? = nil,
                  retry: Int? = nil) -> Self {
        let request: SaltonseaBasicRequest = SaltonseaBasicRequest(requestType: .get, urlCompatiableType: urlCompatiableType, paras: paras, retry: retry)
        saltonseaBasicRequets.push(element: request)
        
        return self
    }
    
    @discardableResult
    open func dataTask(onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                       onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDataTask {
        var task: URLSessionDataTask = URLSessionDataTask()
        guard let request: SaltonseaBasicRequest = saltonseaBasicRequets.pop() else { return task }
        
        task = boot.reuqestCenter(basicRequest: request) { data, response in
            if let data = data, let response = response {
                DispatchQueue.main.async {
                    onSuccess(data, response)
                }
            }
        } onFailure: { error in
            if let error = error {
                DispatchQueue.main.async {
                    onFailure(error)
                }
            }
        }
        
        return task
    }
    
    @discardableResult
    open func sessionTask(onSuccess: @escaping ((_ data: Any?, _ response: Any?) -> Void?),
                     onFailure: @escaping ((_ error: Any?) -> Void?)) -> URLSessionDataTask {
        var task: URLSessionDataTask = URLSessionDataTask()
        guard let request: SaltonseaBasicRequest = saltonseaBasicRequets.pop() else { return task }
        
        self.boot.sessionTaskCenter(saltonseaRequest: saltonseaRequest) { data, response in
            DispatchQueue.main.async {
                if let data = data, let response = response {
                    onSuccess(data, response)
                }
            }
        } onFailure: { error in
            DispatchQueue.main.async {
                if let error = error {
                    onFailure(error)
                }
            }
        }
        
        return task
    }
    
    @discardableResult
    open func setOnProgressListener(_ listener: ((CGFloat) -> Void)?) -> Self {
        self.boot.setOnProgressListener { rate in
            listener?(rate)
        }
        
        return self
    }
    
    @discardableResult
    open func setOnStatusListener(listener: ((_ code: Int) -> Void)?) -> URLSessionDataTask {
        var task: URLSessionDataTask = URLSessionDataTask()
        guard let type: URLCompatibleType = urlCompatiables.pop() else { return task }
        guard let request: SaltonseaBasicRequest = saltonseaBasicRequets.pop() else { return task }
        
        task = boot.setOnStatusListener(urlCompatiableType: type) { code in
            listener?(code)
        }
        
        return task
    }

}


*/
