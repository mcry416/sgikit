/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// MARK: - URLCompatibleType

import Foundation

// MARK: - SaltonseaBasicRequest

public class SaltonseaBasicRequest: NSObject {
    public var requestType: SaltonseaRequestType
    public var urlCompatiableType: URLCompatibleType
    public var paras: [String: Any]?
    public var retry: Int?
    
    public init(requestType: SaltonseaRequestType, urlCompatiableType: URLCompatibleType, paras: [String : Any]? = nil, retry: Int? = 0) {
        self.requestType = requestType
        self.urlCompatiableType = urlCompatiableType
        self.paras = paras
        self.retry = retry
    }
}

// MARK: - URLCompatibleType

public protocol URLCompatibleType { }

extension String: URLCompatibleType { }
extension URL: URLCompatibleType { }

// MARK: - SaltonseaContentType

public enum SaltonseaContentType: String {
    case json       = "application/json"
    case xml        = "application/xml"
    case urlencoded = "application/x-www-form-urlencoded"
    case stream     = "application/octet-stream"
    case pdf        = "application/pdf"
    case formData   = "multipart/form-data"
    
}

// MARK: - SaltonseaRequestType

public enum SaltonseaRequestType: String {
    case post    = "POST"
    case get     = "GET"
    case put     = "PUT"
    case delete  = "DELETE"
    case head    = "HEAD"
    case options = "OPTIONS"
    case patch   = "PATCH"
}

// MARK: - SaltonTaskType

public enum SaltonTaskType: Int {
    case download  = 0
    case upload    = 1
}

// MARK: - SaltonseaError

public enum SaltonseaError: String {
    case urlError01 = "Unresolved URL type."
}

// MARK: - SaltonseaStatusCode

internal enum SaltonseaStatusCode: String {
    case code_100 = "continue"
    case code_101 = "switching protocols"
    
    case code_200 = "ok"
    case code_201 = "created"
    case code_202 = "accepted"
    case code_203 = "non authoritative info"
    case code_204 = "no content"
    case code_205 = "reset content"
    case code_206 = "partial content"
    
    case code_300 = "multiple choices"
    case code_301 = "moved permanently"
    case code_302 = "found"
    case code_303 = "see other"
    case code_304 = "not modified"
    case code_305 = "use proxy"
    case code_306 = "switch proxy"
    case code_307 = "temporary redirect"
    case coed_308 = "permanent redirect"

    case code_400 = "bad request"
    case code_401 = "unauthorized"
    case code_402 = "payment required"
    case code_403 = "forbidden"
    case code_404 = "not found"
    case code_405 = "method not allowed"
    case code_406 = "not acceptable"
    case code_407 = "proxy authentication required"
    case code_408 = "request timeout"
    case code_409 = "conflict"
    case code_410 = "gone"
    case code_411 = "length required"
    case code_412 = "precondition failed"
    case code_413 = "request entity too large"
    case code_414 = "request uri too large"
    case code_415 = "unsupported media type"
    case code_416 = "requested range not satisfiable"
    case code_417 = "expectation failed"
    
    case code_500 = "internal server error"
    case code_501 = "not implemented"
    case code_502 = "bad gateway"
    case code_503 = "service unavailable"
    case code_504 = "gateway timeout"
    case code_505 = "http version not supported"
    
}


