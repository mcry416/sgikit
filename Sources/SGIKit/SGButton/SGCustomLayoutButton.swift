/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

open class SGCustomLayoutButton: UIButton {

    public enum ImagePosition {
        case left
        case right
        case top
        case bottom
    }
    
    open var imagePosition: ImagePosition = .left
    open var imagePadding: CGFloat = 0
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let imageView = imageView, let titleLabel = titleLabel else { return }
        
        switch imagePosition {
        case .left:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -imagePadding/2, bottom: 0, right: imagePadding/2)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: imagePadding/2, bottom: 0, right: -imagePadding/2)
            
        case .right:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: titleLabel.frame.width + imagePadding/2, bottom: 0, right: -titleLabel.frame.width - imagePadding/2)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageView.frame.width - imagePadding/2, bottom: 0, right: imageView.frame.width + imagePadding/2)
            
        case .top:
            imageEdgeInsets = UIEdgeInsets(top: -titleLabel.frame.height - imagePadding/2, left: 0, bottom: 0, right: -titleLabel.frame.width)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageView.frame.width, bottom: -imageView.frame.height - imagePadding/2, right: 0)
            
        case .bottom:
            imageEdgeInsets = UIEdgeInsets(top: titleLabel.frame.height + imagePadding/2, left: 0, bottom: 0, right: -titleLabel.frame.width)
            titleEdgeInsets = UIEdgeInsets(top: -imageView.frame.height - imagePadding/2, left: -imageView.frame.width, bottom: 0, right: 0)
        }
    }

}
