/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

open class SGContextMenuView: UIView {
    
    private var superPoint: CGPoint = .zero

    public var contentEdge: CGFloat = 115
    public var edgeOffset: CGFloat = 3
    
    private var didClickBlock: ((IndexPath) -> Void)?
    private let CELL_ID: String = "SGContextMenuView_CELL_ID"
    private(set) var datas: Array<SGContextMenuSectionModel> = Array<SGContextMenuSectionModel>()

    private lazy var contentView: UIView = UIView()
    private lazy var decorateView: UIView = UIView()
    private lazy var tableView = UITableView(frame: .zero, style: .plain)

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = UIScreen.main.bounds
        self.addSubview(decorateView)
        decorateView.addSubview(contentView)
        contentView.addSubview(tableView)
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CELL_ID)
        decorateView.alpha = 0
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        self.hide()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = decorateView.bounds
        tableView.frame = decorateView.bounds
        
        contentView.backgroundColor = UIColor.white
        contentView.backgroundColor = .clear
        contentView.layer.cornerRadius = 8
        contentView.layer.masksToBounds = true
        
        decorateView.layer.shadowColor = UIColor.black.cgColor
        decorateView.layer.shadowOffset = CGSizeMake(8, 8)
        decorateView.layer.shadowRadius = 32
        decorateView.layer.shadowOpacity = 0.5
        decorateView.layer.shadowPath = UIBezierPath(rect: decorateView.bounds).cgPath
    }
    
}

// MARK: - Animation & Event
extension SGContextMenuView {
    
    public func setOnSelectListener(_ handler: ((IndexPath) -> Void)?) {
        didClickBlock = handler
    }
    
    public func setData(_ data: Array<SGContextMenuSectionModel>) {
        self.datas = data
        tableView.reloadData()
    }
    
    public func show(at superContentView: UIView?) {
        guard let superContentView = superContentView else { return }
        guard let window: UIWindow = UIApplication.shared.keyWindow else { return }
        window.addSubview(self)
        self.addSubview(decorateView)
        
        setupFrame(withSuperView: superContentView)
    }
    
    public func hide() {
        
        UIView.animate(withDuration: 0.3) {
            self.decorateView.alpha = 0
        } completion: { (res) in
            self.removeFromSuperview()
        }
    }
    
    private func setupFrame(withSuperView view: UIView) {
        let width: CGFloat = 150
        let sectionH: CGFloat = CGFloat((datas.count - 1) * 10)
        var cellsCount: Int = 0
        var cellsH: CGFloat = 0
        for section in datas {
            for subMenuModel in section.menuModels {
                cellsCount = cellsCount + 1
                let isOverW120: Bool = textW(subMenuModel.title) > 120
                subMenuModel.height = isOverW120 ? 33 * 2 : 33
                cellsH = cellsH + subMenuModel.height
            }
        }
        let height: CGFloat = sectionH + cellsH
        var y: CGFloat = view.frame.maxY + edgeOffset
        var x: CGFloat = view.frame.midX
        let convertPoint: CGPoint = view.convert(CGPoint(x: x, y: y), to: self)
        y = convertPoint.y
        
        x = (x + width > self.bounds.width) ? self.bounds.width - width - 16 : x
        y = (y + height > self.bounds.height) ? self.bounds.height - height - 16 : y
        
        let rect: CGRect = CGRect(x: x, y: y, width: width, height: height)
        self.superPoint = CGPoint(x: x, y: y)
        self.decorateView.frame = CGRect(x: x, y: y, width: 0, height: 0)
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0) {
            self.decorateView.frame = rect
            self.decorateView.alpha = 1
        }
    }
    
    private func textsMaxW(_ texts: Array<String>) -> CGFloat {
        return texts.map {
            $0.boundingRect(with:CGSize(width: CGFLOAT_MAX, height: 20), options: [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context:nil).width
        }.max() ?? 0
    }
    
    private func textW(_ text: String) -> CGFloat {
        ceil(text.boundingRect(with:CGSize(width: CGFLOAT_MAX, height: 20), options: [NSStringDrawingOptions.usesLineFragmentOrigin, NSStringDrawingOptions.usesFontLeading], attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context:nil).width)
    }
    
}

// MARK: - Delegate
extension SGContextMenuView: UITableViewDelegate, UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        datas.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        datas[section].menuModels.count
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == datas.count - 1{
            return nil
        } else {
            let view = UIView()
            if #available(iOS 13.0, *) {
                view.backgroundColor =  UIColor.systemGray6
            } else {
                view.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 0)
            }
            return view
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == (datas.count - 1){
            return 0
        } else {
            return 10
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath)
        
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = datas[indexPath.section].menuModels[indexPath.row].title
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
        let imageName = datas[indexPath.section].menuModels[indexPath.row].imageName
        if imageName != "" {
            cell.imageView?.image = UIImage(named: imageName)
        }
        if let color = datas[indexPath.section].menuModels[indexPath.row].color {
            cell.textLabel?.textColor = color
        }
        cell.contentView.alpha = datas[indexPath.section].menuModels[indexPath.row].isEnable ? 1 : 0.5
        
        let seprator: UIView = UIView()
        seprator.backgroundColor = UIColor(red: 0.62, green: 0.61, blue: 0.60, alpha: 1)
        cell.contentView.addSubview(seprator)
        seprator.gram.layout { make in
            make.left.equalTo(cell.contentView.gram.left)
            make.right.equalTo(cell.contentView.gram.right)
            make.bottom.equalTo(cell.contentView.gram.bottom)
            make.height.equalTo(CGFloat(0.5))
        }
        if datas[indexPath.section].menuModels.count == (indexPath.row + 1){
            seprator.removeFromSuperview()
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didClickBlock?(indexPath)
        hide()
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        datas[indexPath.section].menuModels[indexPath.row].height
    }
    
}

public class SGContextMenuSectionModel: NSObject {
    
    public var menuModels: Array<SGContextMenuModel> = Array<SGContextMenuModel>()
    
    public var squareModels: Array<SGContextSquareModel> = Array<SGContextSquareModel>()
}

public class SGContextSquareModel: NSObject {
    
    public var title: String = ""
    public var imageName: String = ""
    public var color: UIColor?
}

public class SGContextMenuModel: NSObject {
    
    public var title: String = ""
    public var imageName: String = ""
    public var color: UIColor?
    public var isEnable: Bool = true
    
    internal var height: CGFloat = 33
}
