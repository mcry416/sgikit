/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

extension UIView {
    
    @discardableResult
    public func setBackgroundColor(_ color: UIColor) -> Self {
        self.backgroundColor = color
        return self
    }
    
    @discardableResult
    public func setFrame(_ frame: CGRect) -> Self {
        self.frame = frame
        return self
    }
    
    @discardableResult
    public func setCornerRadius(_ radius: CGFloat) -> Self {
        self.layer.cornerRadius = radius
        return self
    }
    
    @discardableResult
    public func setMaskToBounds(_ isMaskToBounds: Bool) -> Self {
        self.layer.masksToBounds = isMaskToBounds
        return self
    }
    
    @discardableResult
    public func setUserInteraction(_ isEnable: Bool) -> Self {
        self.isUserInteractionEnabled = isEnable
        return self
    }
    
}

