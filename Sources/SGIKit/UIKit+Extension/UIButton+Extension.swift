/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

extension UIButton {
    
    public func setNativeTitle (_ localKey: String?, for state: UIControl.State) {
        guard let localKey = localKey else {
            print("------> UIButton.setNativeTitle() faile.")
            return
        }
        let value: String = NSLocalizedString(localKey, comment: "")
        self.setTitle(value, for: state)
    }
    
}
