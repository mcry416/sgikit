/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

extension UIImageView {
    
    @discardableResult
    public func setImage(_ imageName: String) -> Self {
        if let img = UIImage(named: imageName) {
            self.image = img
        }
        return self
    }
    
}

