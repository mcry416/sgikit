/**
 * Copyright mcry416(mcry416@outlook.com). and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import UIKit

extension UILabel {
    
    public var nativeText: String? {
        set {
            if let temp = newValue {
                text = NSLocalizedString(temp, comment: "")
            } else {
                print("------> UILabel loaded nativeText faile.")
            }
        }
        get { text }
    }
    
}

extension UILabel {
    
    @discardableResult
    public func setText(_ text: String?) -> Self {
        if let text = text {
            self.text = text
        }
        return self
    }
    
    @discardableResult
    public func setFont(_ font: UIFont) -> Self {
        self.font = font
        return self
    }
    
    @discardableResult
    public func setTextColor(_ textColor: UIColor) -> Self {
        self.textColor = textColor
        return self
    }
    
    @discardableResult
    public func setTextAlignment(_ textAlignment: NSTextAlignment) -> Self {
        self.textAlignment = textAlignment
        return self
    }
    
    @discardableResult
    public func setAttr(_ attr: NSAttributedString?) -> Self {
        if let attr = attr {
            self.attributedText = attr
        }
        return self
    }
    
}

