// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SGIKit",
    products: [
        .library(
            name: "SGIKit",
            targets: ["SGIKit"]),
    ],
    
    dependencies: [
    ],
    
    targets: [
        .target(
            name: "SGIKit",
            dependencies: [],
            resources: [

            ]
        ),
    ]
    
)
